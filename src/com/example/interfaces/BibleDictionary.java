/**
 * 
 */
package com.example.interfaces;

/**
 * @author Vanessa_CU
 *
 */
public interface BibleDictionary {
	public String reverse(String line);
	public String findReference(String reference);
	public void buildStructure();
}
