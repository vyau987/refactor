package com.example;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import com.example.interfaces.BibleDictionary;


// 1. must read a large file
// 2. construct an index structure representing all the lines
// 3. enable us to find a particular line (in our app, this will be done a lot!)
// 4. reverse every other line
// 5. print some reversed lines
// 6. instrument some profiling so you know what aspects of the application take the most time
public class RefactoredParser implements BibleDictionary{
	private String text;
	private LinkedHashMap map = new LinkedHashMap<String, String>();
	
	public RefactoredParser(String filepath) {
		this.text = filepath;
	}
	
	public static void main(String[] args) {
		Long start = System.currentTimeMillis();
	    System.out.println("Time: 0 milliseconds");
		RefactoredParser parser = new RefactoredParser("bible.txt");
		try {
			parser.buildStructure();
		} catch (Exception e) {
			e.printStackTrace();
		}
	    System.out.println("Time: "+(System.currentTimeMillis()-start) + " milliseconds");
		
	    System.out.println( parser.map.get("John 11:35"));
		parser.findReference("John 11:35");
		
	    
	    // find the reference (ignore case)
		parser.findReference("Matthew 1:1");
		parser.findReference("1 Timothy 4:3");
	    System.out.println("Time: "+(System.currentTimeMillis()-start) + " milliseconds");
		    
        // print a few lines that should be reversed
	    parser.getByIndex(0);
	    parser.getByIndex(2);
	    System.out.println("Time: "+(System.currentTimeMillis()-start) + " milliseconds");
	}
	
	@Override
	public void buildStructure() {
		try{
			// read the file
			Scanner reader = new Scanner(new File(text));
			
			boolean reverseBool = true;
			String verse;
			while( reader.hasNextLine() ) {
				verse = reader.nextLine();
//				System.out.println("Reading line: "+verse + " milliseconds");

				String[] aLine = verse.split("\t");
				if (reverseBool) 
					map.put(aLine[0], reverse(verse));
				else 
					map.put(aLine[0], verse);
				
				reverseBool = !reverseBool;
//				System.out.println("Adding line: " + verse + " milliseconds");
			}			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.example.interfaces.BibleDictionary#findReference(java.lang.String)
	 */
	@Override
	public String findReference(String reference) {
		String verse = (String) map.get(reference);
		if (null != verse) {
			System.out.println("Found line: "+ verse);
		} else {
			System.out.println("Line not found.");
		}
		return verse;
	}

	public void getByIndex(int index) {
		List<List<String>> l = new ArrayList<List<String>>(map.values());
		System.out.println("By index: "+l.get(index));
	}

	/* (non-Javadoc)
	 * @see com.example.interfaces.BibleDictionary#reverse(java.lang.String)
	 */
	@Override
	public String reverse(String line) {
		return new StringBuilder(line).reverse().toString();
	}
	
}
