package com.example;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Vector;


// 1. must read a large file
// 2. construct an index structure representing all the lines
// 3. enable us to find a particular line (in our app, this will be done a lot!)
// 4. reverse every other line
// 5. print some reversed lines
// 6. instrument some profiling so you know what aspects of the application take the most time
public class Parser {
	public static void main(String[] args) throws Exception {
	
		
		// read the file
	    FileInputStream a__ = new FileInputStream("bible.txt");
	    Reader a_ = new BufferedReader(new InputStreamReader(a__));
        String _s = "";
        char[] buffer = new char[1024];
        int z = 0;
        int anInt;
        while ((anInt = a_.read(buffer, 0, buffer.length)) > z) {
        	System.out.println("Reading line: " + new String(buffer, z, anInt));
        	_s += new String(buffer, 0, anInt);
        }
        
        // split to lines
        String[] arrs = _s.split("\r\n");
        Vector<String> variable = new Vector();
        for (String line : arrs) {
        	System.out.println("Adding line: " + line);
        	variable.add(line);
        }
        
        // find the reference (ignore case)
        String result = "Matthew 1:1";
        String query = "";
        for (String ___s: arrs) {
        	if (___s.toLowerCase().startsWith(result.toLowerCase())) {
        		query = ___s;
        	}
        }
        
        System.out.println("Found line: " + query);
        
        // find the reference (ignore case)
        result = "1 Timothy 4:3";
        query = "";
        for (String ___s: arrs) {
        	if (___s.toLowerCase().startsWith(result.toLowerCase())) {
        		query = ___s;
        	}
        }
        
        System.out.println("Found line: " + query);
	    
	    // reverse the first line, and then every other line
	    boolean reverse = true;
	    boolean lastReversed = !reverse;
	    
	    // work out how many lines to reverse
	    // reverse half the number of lines (div by 2)
	    int numToReverse = variable.size() / 2;
	    for (int i=0; i < numToReverse; i++) {
	    	lastReversed = reverse ? !lastReversed : false;
	    	if (!lastReversed) {
	    		variable.set(i, variable.get(i));
	    	}
	    	else {
	    		String line = variable.get(i);
	    		String reversedLine = "";
	    		for (int j=line.length()-1; j >= 0; j--) {
	    			reversedLine += line.charAt(j);
	    		}
	    		variable.set(i, reversedLine);
	    	}
	    }
	    
	    // print a few lines that should be reversed
	    System.out.println(variable.get(0));
	    System.out.println(variable.get(2));
	    
	}	
}
