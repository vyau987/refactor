/**
 * 
 */
package com.example.test;

import org.junit.Assert;
import org.junit.Test;

import com.example.RefactoredParser;
import com.example.interfaces.BibleDictionary;

/**
 * @author Vanessa_CU
 *
 */
public class Tester {
	private BibleDictionary dictionary;
	
	public Tester() {
		this.dictionary = new RefactoredParser("bible.txt");
		this.dictionary.buildStructure();
	}
	
	
	@Test
	public void testReverseLine() {
		Assert.assertEquals("cba", dictionary.reverse("abc"));
		Assert.assertEquals("", dictionary.reverse(""));
	}
	
	@Test
	public void testFindReference() {
		Assert.assertEquals(".tpew suseJ	53:11 nhoJ",
				dictionary.findReference("John 11:35"));
		Assert.assertNull(		dictionary.findReference(""));
		Assert.assertNotNull(	dictionary.findReference("Matthew 1:1"));
		
	}
	
}
